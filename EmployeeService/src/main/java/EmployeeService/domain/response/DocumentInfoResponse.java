package EmployeeService.domain.response;

import EmployeeService.domain.common.ResponseStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Builder
public class DocumentInfoResponse {
    private ResponseStatus status;
    private String path;
    private String fileName;
    private Date lastModificationDate;

}
