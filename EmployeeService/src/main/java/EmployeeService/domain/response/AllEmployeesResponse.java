package EmployeeService.domain.response;

import EmployeeService.domain.Employee;
import EmployeeService.domain.common.ResponseStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class AllEmployeesResponse {
    private ResponseStatus status;
    private List<Employee> employees;
}
