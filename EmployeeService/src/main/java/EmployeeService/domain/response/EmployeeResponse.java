package EmployeeService.domain.response;

import EmployeeService.domain.Employee;
import EmployeeService.domain.common.ResponseStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class EmployeeResponse {
    private ResponseStatus status;
    private Employee employee;
}