package EmployeeService.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import java.util.Date;
import java.util.List;

@Document(collection = "employee")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Employee {
    @Id
    private String id;

    @Indexed(unique = true)
    private String userId;

    private String firstName;

    private String lastName;

    private String middleName;

    private String preferredName;

    private String email;

    private String cellPhone;

    private String alternatePhone;

    private String gender;

    private String ssn;

    private Date dob;

    private Date startDate;

    private Date endDate;

    private String driverLicense;

    private Date driverLicenseExpiration;

    private Integer houseId;

    private List<Contact> contactList;

    private List<Address> addressList;

    private List<VisaStatus> visaStatusList;

    private List<PersonalDocument> personalDocumentList;



}
