package EmployeeService.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "address")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Address {
    @Id
    private String id;

    private String addressLine1;

    private String addressLine2;

    private String city;

    private String state;

    private String zipCode;
}
