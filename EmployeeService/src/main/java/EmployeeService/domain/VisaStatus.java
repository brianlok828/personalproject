package EmployeeService.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "visaStatus")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class VisaStatus {
    @Id
    private String id;

    private String visaType;

    private boolean activeFlag;

    private Date startDate;

    private Date endDate;

    private Date lastModificationDate;
}
