package EmployeeService.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contact")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Contact {
    @Id
    private String id;

    private String firstName;

    private String lastName;

    private String cellPhone;

    private String alternatePhone;

    private String email;

    private String relationship;

    private String type;
}
