package EmployeeService.domain.request;

import EmployeeService.domain.Address;
import EmployeeService.domain.Contact;
import EmployeeService.domain.PersonalDocument;
import EmployeeService.domain.VisaStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
public class EmployeeRequest {

    @NotBlank(message = "user id is required")
    private String userId;

    @NotBlank(message = "first name is required")
    private String firstName;

    @NotBlank(message = "last name is required")
    private String lastName;

    private String middleName;

    private String preferredName;

    @NotBlank(message = "email is required")
    @Email
    private String email;

    @NotBlank(message = "cell phone is required")
    private String cellPhone;

    private String alternatePhone;

    @NotBlank(message = "gender is required")
    private String gender;

    @NotNull(message = "SSN is required")
    private String ssn;

    @NotNull(message = "DOB is required")
    private Date dob;

    @NotNull(message = "start date is required")
    private Date startDate;

    private Date endDate;

    private String driverLicense;

    private Date driverLicenseExpiration;

    private Integer houseId;

    @NotEmpty
    private List<Contact> contactList;

    @NotEmpty
    private List<Address> addressList;

    private List<VisaStatus> visaStatusList;

    private List<PersonalDocument> personalDocumentList;
}
