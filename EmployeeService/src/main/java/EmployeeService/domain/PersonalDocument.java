package EmployeeService.domain;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.nio.file.Path;
import java.util.Date;

@Document(collection = "personal-document")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PersonalDocument {
    @Id
    private ObjectId id;

    //Path not working
    private String path;

    private String title;

    private String comment;

    private Date createDate;
}
