package EmployeeService.representation;

import lombok.Data;

@Data
public class BucketObjectRepresentation {
    private String objectName;
    private String text;
}
