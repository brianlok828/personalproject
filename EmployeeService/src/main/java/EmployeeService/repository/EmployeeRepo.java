package EmployeeService.repository;

import EmployeeService.domain.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepo extends MongoRepository<Employee, String> {

    Employee findEmployeeById(String id);

}
