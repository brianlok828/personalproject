package EmployeeService.repository;

import EmployeeService.domain.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeePagingRepo extends PagingAndSortingRepository<Employee, Integer> {
}
