package EmployeeService.controller;

import EmployeeService.domain.PersonalDocument;
import EmployeeService.domain.response.DocumentInfoResponse;
import EmployeeService.service.EmployeeService;
import EmployeeService.service.FileService;
import com.amazonaws.services.s3.model.Bucket;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FileController {

    @Autowired
    private FileService fileService;

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("buckets")
    public List<String> listBuckets() {
        List<Bucket> buckets = fileService.listBuckets();
        List<String> bucketNames = buckets.stream().map(Bucket::getName).collect(Collectors.toList());
        return bucketNames;
    }

    @GetMapping("documents")
    public List<String> getAllDocuments(@RequestParam String bucketName) {
        return fileService.getAllDocuments(bucketName);
    }

    @GetMapping("document/{bucketName}/{fileName}")
    public File downloadFile(@PathVariable String bucketName, @PathVariable String fileName) throws IOException {
        fileService.downloadObject(bucketName, fileName);
        return new File("./" + fileName);
    }

    @PostMapping("document/{bucketName}/upload")
    public DocumentInfoResponse uploadFile(@PathVariable String bucketName, @RequestParam String fileName, @RequestParam MultipartFile file, @RequestParam String employeeId) throws IOException {
        PersonalDocument personalDocument = fileService.uploadObject(bucketName, fileName, file);
        employeeService.addPersonalDocumentByEmployeeId(employeeId, personalDocument);
        return fileService.getDocumentInfo(bucketName, fileName);
    }

    @GetMapping("document/{bucketName}/{fileName}/info")
    public DocumentInfoResponse getDocumentInfo(@PathVariable String bucketName, @PathVariable String fileName) {
        return fileService.getDocumentInfo(bucketName, fileName);
    }

}