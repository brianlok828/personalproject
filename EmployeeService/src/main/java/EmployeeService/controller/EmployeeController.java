package EmployeeService.controller;

import EmployeeService.domain.Employee;
import EmployeeService.domain.common.ResponseStatus;
import EmployeeService.domain.request.EmployeeRequest;
import EmployeeService.domain.response.AllEmployeesResponse;
import EmployeeService.domain.response.EmployeeResponse;
import EmployeeService.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class EmployeeController {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/employees")
    public AllEmployeesResponse getAllEmployees() {
        return AllEmployeesResponse.builder()
                .status(
                        ResponseStatus.builder()
                                .success(true)
                                .message("Returning all employees")
                                .build()
                )
                .employees(employeeService.findAllEmployees())
                .build();
    }

    @GetMapping("/employees/page/{page}")
    public AllEmployeesResponse getAllEmployeeByPage(@PathVariable int page,
                                                     @RequestParam(defaultValue = "5") int size,
                                                     @RequestParam(defaultValue = "lastName") String sortBy) {
        return AllEmployeesResponse.builder()
                .status(
                        ResponseStatus.builder()
                                .success(true)
                                .message("Returning employee page " + page)
                                .build()
                )
                .employees(employeeService.findAllEmployeesByPageAndSort(page, size, sortBy))
                .build();
    }

    @GetMapping("/employees/{houseId}")
    public AllEmployeesResponse getEmployeeByHouseId(@PathVariable Integer houseId) {
        return AllEmployeesResponse.builder()
                .status(
                        ResponseStatus.builder()
                                .success(true)
                                .message("Returning employee with same houseID")
                                .build()
                )
                .employees(employeeService.findEmployeeByHouseId(houseId))
                .build();
    }

    @GetMapping("/employee/{employeeId}")
    public EmployeeResponse getEmployeeById(@PathVariable String employeeId) {
        List<Employee> employees = employeeService.findAllEmployees();

        Optional<Employee> employeeOptional = employees.stream()
                .filter(employee -> employee.getId().equals(employeeId))
                .findFirst();

        if (employeeOptional.isPresent()) {
            return EmployeeResponse.builder()
                    .status(
                            ResponseStatus.builder()
                                    .success(true)
                                    .message("Success! Employee was found")
                                    .build()
                    )
                    .employee(employeeOptional.get())
                    .build();
        } else {
            return EmployeeResponse.builder()
                    .status(
                            ResponseStatus.builder()
                                    .success(false)
                                    .message("Oops, employee not found")
                                    .build()
                    )
                    .build();
        }
    }

    @PutMapping("/employee/{employeeId}")
    public EmployeeResponse updateEmployeeById(@PathVariable String employeeId, @RequestBody Employee updatedEmployee) {
        List<Employee> employees = employeeService.findAllEmployees();

        Optional<Employee> employeeOptional = employees.stream()
                .filter(employee -> employee.getId().equals(employeeId))
                .findFirst();

        if (employeeOptional.isPresent()) {
            //save update employee
            employeeService.saveOrUpdateEmployee(updatedEmployee);

            return EmployeeResponse.builder()
                    .status(
                            ResponseStatus.builder()
                                    .success(true)
                                    .message("Success! Employee was updated")
                                    .build()
                    )
                    .employee(updatedEmployee)
                    .build();
        } else {
            return EmployeeResponse.builder()
                    .status(
                            ResponseStatus.builder()
                                    .success(false)
                                    .message("Oops, employee not found")
                                    .build()
                    )
                    .build();
        }
    }


    @PostMapping("/employee/{employeeId}/assignHouse")
    public EmployeeResponse assignHouseToEmployee(@PathVariable String employeeId, @RequestParam Integer houseId) {
        List<Employee> employees = employeeService.findAllEmployees();

        Optional<Employee> employeeOptional = employees.stream()
                .filter(employee -> employee.getId().equals(employeeId))
                .findFirst();

        if (employeeOptional.isPresent()) {
            //save update employee
            employeeService.updateHouseIdByEmployeeId(employeeId, houseId);

            return EmployeeResponse.builder()
                    .status(
                            ResponseStatus.builder()
                                    .success(true)
                                    .message("Success! Employee was updated")
                                    .build()
                    )
                    .employee(employeeService.findEmployeeById(employeeId))
                    .build();
        } else {
            return EmployeeResponse.builder()
                    .status(
                            ResponseStatus.builder()
                                    .success(false)
                                    .message("Oops, employee not found")
                                    .build()
                    )
                    .build();
        }
    }

    @PostMapping("/createEmployee")
    public EmployeeResponse createNewEmployee(@Valid @RequestBody EmployeeRequest employee, BindingResult bindingResult){
        // perform validation check
        if (bindingResult.hasErrors()) {
            List<FieldError> errors = bindingResult.getFieldErrors();
            errors.forEach(error -> System.out.println(
                    "ValidationError in " + error.getObjectName() + ": " + error.getDefaultMessage()));
            return EmployeeResponse.builder()
                    .status(
                            ResponseStatus.builder()
                                    .success(false)
                                    .message("Validation error")
                                    .build()
                    )
                    .build();
        }
        // validation passed, create new book
        Employee newEmployee = Employee.builder()
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .middleName(employee.getMiddleName())
                .email(employee.getEmail())
                .userId(employee.getUserId())
                .preferredName(employee.getPreferredName())
                .cellPhone(employee.getCellPhone())
                .alternatePhone(employee.getAlternatePhone())
                .gender(employee.getGender())
                .ssn(employee.getSsn())
                .dob(employee.getDob())
                .startDate(employee.getStartDate())
                .endDate(employee.getEndDate())
                .driverLicense(employee.getDriverLicense())
                .driverLicenseExpiration(employee.getDriverLicenseExpiration())
                .houseId(employee.getHouseId())
                .addressList(employee.getAddressList())
                .contactList(employee.getContactList())
                .visaStatusList(employee.getVisaStatusList())
                .personalDocumentList(employee.getPersonalDocumentList())

                .build();

        // save new employee to database
        employeeService.saveOrUpdateEmployee(newEmployee);

        return EmployeeResponse.builder()
                .status(
                        ResponseStatus.builder()
                                .success(true)
                                .message("New employee created")
                                .build()
                )
                .employee(newEmployee)
                .build();
    }

}
