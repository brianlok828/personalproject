package EmployeeService.service;

import EmployeeService.domain.Employee;
import EmployeeService.domain.PersonalDocument;
import EmployeeService.domain.VisaStatus;
import EmployeeService.repository.EmployeePagingRepo;
import EmployeeService.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    private final EmployeeRepo employeeRepo;
    private final EmployeePagingRepo employeePagingRepo;

    public EmployeeService(EmployeeRepo employeeRepo, EmployeePagingRepo employeePagingRepo) {
        this.employeeRepo = employeeRepo;
        this.employeePagingRepo = employeePagingRepo;
    }

    public List<Employee> findAllEmployees() {
        return employeeRepo.findAll();
    }

    public List<Employee> findAllEmployeesByPageAndSort(int page, int size, String sortBy) {
        return employeePagingRepo.findAll(PageRequest.of(page, size, Sort.by(sortBy))).getContent();
    }

    public List<Employee> findEmployeeByHouseId(Integer houseId) {
        List<Employee> employeeWithSameHouseId = findAllEmployees().stream()
                .filter(e -> e.getHouseId() != null)
                .filter(e -> e.getHouseId().equals(houseId))
                .collect(Collectors.toList());
        return employeeWithSameHouseId;
    }

    public Employee findEmployeeById(String id) {
        return employeeRepo.findEmployeeById(id);
    }

    public void saveOrUpdateEmployee(Employee employee) {
        employeeRepo.save(employee);
    }

    public void addVisaStatusByEmployeeId(String id, VisaStatus visaStatus) {
        Employee employee = employeeRepo.findEmployeeById(id);
        List<VisaStatus> visaStatusList = employee.getVisaStatusList();
        visaStatusList.add(visaStatus);
        employee.setVisaStatusList(visaStatusList);
        employeeRepo.save(employee);
    }

    public void addPersonalDocumentByEmployeeId(String id, PersonalDocument personalDocument) {
        Employee employee = employeeRepo.findEmployeeById(id);
        List<PersonalDocument> personalDocumentList = employee.getPersonalDocumentList();
        personalDocumentList.add(personalDocument);
        employee.setPersonalDocumentList(personalDocumentList);
        employeeRepo.save(employee);
    }

    public void updateHouseIdByEmployeeId(String id, Integer houseId) {
        Employee employee = employeeRepo.findEmployeeById(id);
        employee.setHouseId(houseId);
        employeeRepo.save(employee);
    }
}
