package EmployeeService.service;

import EmployeeService.domain.PersonalDocument;
import EmployeeService.domain.common.ResponseStatus;
import EmployeeService.domain.response.DocumentInfoResponse;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FileService {

    private static final Logger LOG = LoggerFactory.getLogger(FileService.class);

    @Autowired
    private AmazonS3 amazonS3;

//    @Value("${s3.bucket.name")
//    private String s3BucketName;

    public List<Bucket> listBuckets() {
        return amazonS3.listBuckets();
    }

    public PersonalDocument uploadObject(String bucketName, String fileName, MultipartFile file) throws IOException {
//        String tempFileName = UUID.randomUUID() + file.getName();
//        File tempFile = new File(System.getProperty("java.io.tmpdir") + "/" + tempFileName);
//        file.transferTo(tempFile); // Convert multipart file to File
//        amazonS3.putObject(bucketName, fileName, tempFile); // Upload file
//        tempFile.deleteOnExit(); //delete temp file
        String tempFileName = UUID.randomUUID() + file.getName();
        File tempFile = new File(System.getProperty("java.io.tmpdir") + "/" + tempFileName);
        file.transferTo(tempFile); // Convert multipart file to File
        amazonS3.putObject(bucketName, fileName, tempFile); // Upload file
        tempFile.deleteOnExit(); //delete temp file

        return PersonalDocument.builder()
                .title(fileName)
                .path(String.valueOf(amazonS3.getUrl(bucketName, fileName)))
                .createDate(amazonS3.getObject(bucketName, fileName).getObjectMetadata().getLastModified())
                .build();
    }

//    public ResponseEntity uploadDocument(@RequestParam(value = "file") MultipartFile file) throws IOException


    public List<String> getAllDocuments(String bucketName){

        return amazonS3.listObjectsV2(bucketName).getObjectSummaries().stream()
                .map(S3ObjectSummary::getKey)
                .collect(Collectors.toList());


    }

    public DocumentInfoResponse getDocumentInfo(String bucketName, String fileName) {
        String  url = String.valueOf(amazonS3.getUrl(bucketName, fileName));
//        amazonS3.listObjectsV2(bucketName).getObjectSummaries().stream()
        return DocumentInfoResponse.builder()
                .status(
                        ResponseStatus.builder()
                                .success(true)
                                .message("Returning document info")
                                .build()
                )
                .path(url)
                .fileName(fileName)
                .lastModificationDate(amazonS3.getObject(bucketName, fileName).getObjectMetadata().getLastModified())
                .build();
    }

    public void downloadObject(String bucketName, String objectName){
        S3Object s3object = amazonS3.getObject(bucketName, objectName);
        S3ObjectInputStream inputStream = s3object.getObjectContent();
        try {
            FileUtils.copyInputStreamToFile(inputStream, new File("." + File.separator + objectName));
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    public void deleteObject(String bucketName, String objectName){
        amazonS3.deleteObject(bucketName, objectName);
    }
}
