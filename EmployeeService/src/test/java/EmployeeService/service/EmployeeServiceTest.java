package EmployeeService.service;


import EmployeeService.domain.Employee;
import EmployeeService.domain.response.AllEmployeesResponse;
import EmployeeService.repository.EmployeeRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {
    @Mock
    private EmployeeRepo employeeRepo;

    @InjectMocks
    private EmployeeService employeeService;

    @Test
    public void test_getAllEmployee() {
        List<Employee> expected = employeeService.findAllEmployees();

        Mockito.when(employeeRepo.findAll()).thenReturn(expected);
        assertEquals(expected, employeeService.findAllEmployees());
    }

}
