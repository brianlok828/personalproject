package EmployeeService.controller;

import EmployeeService.domain.Employee;
import EmployeeService.domain.common.ResponseStatus;
import EmployeeService.domain.response.EmployeeResponse;
import EmployeeService.repository.EmployeeRepo;
import EmployeeService.security.JwtFilter;
import EmployeeService.security.JwtProvider;
import EmployeeService.service.EmployeeService;
import com.google.gson.Gson;
import lombok.Data;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(controllers = EmployeeController.class)
@AutoConfigureDataMongo
@AutoConfigureMockMvc(addFilters = false)
public class TestEmployeeController {
    @MockBean
    EmployeeService employeeService;

    @MockBean
    JwtFilter jwtFilter;

    @MockBean
    JwtProvider jwtProvider;


    @Autowired
    private MockMvc mockMvc;


    @Test
    void test_getEmployeeById() throws Exception {


        EmployeeResponse expected = EmployeeResponse.builder()
                .status(
                        ResponseStatus.builder()
                                .success(true)
                                .message("")
                                .build()
                )
                .employee(null)
                .build();

        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.get("/employee/{employeeId}", "1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        System.out.println(result);
        Gson gson = new Gson();
        EmployeeResponse actual = gson.fromJson(result.getResponse().getContentAsString(), EmployeeResponse.class);
        assertEquals(expected.getEmployee(), actual.getEmployee());



    }
}
