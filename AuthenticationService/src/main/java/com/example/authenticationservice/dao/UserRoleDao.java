package com.example.authenticationservice.dao;

import com.example.authenticationservice.entity.UserRole;
import org.springframework.stereotype.Repository;


@Repository
public class UserRoleDao extends AbstractHibernateDao<UserRole>{
    public UserRoleDao() { setClazz(UserRole.class);}

}
