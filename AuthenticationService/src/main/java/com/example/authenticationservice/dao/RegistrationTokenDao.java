package com.example.authenticationservice.dao;

import com.example.authenticationservice.entity.RegistrationToken;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class RegistrationTokenDao extends AbstractHibernateDao<RegistrationToken>{

    public RegistrationTokenDao() { setClazz(RegistrationToken.class);}

    public List<RegistrationToken> getAllRegistrationTokens() {
        List<RegistrationToken> lst = this.getAll();
        return lst.stream()
                .map(u -> (RegistrationToken)u)
                .collect(Collectors.toList());
    }

    public Optional<RegistrationToken> getRegistrationTokenByToken(String token) {
        CriteriaBuilder cb = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<RegistrationToken> cr = cb.createQuery(RegistrationToken.class);
        Root<RegistrationToken> root = cr.from(RegistrationToken.class);
        cr.select(root);
        cr.where(cb.equal(root.get("token"), token));

        Query<RegistrationToken> query = getCurrentSession().createQuery(cr);
        List<RegistrationToken> temp = query.getResultList();
        return temp.stream().findAny();
    }

    public void deleteRegistrationToken(String token) {
        Optional<RegistrationToken> regToken = getRegistrationTokenByToken(token);
        getCurrentSession().delete(regToken.get());
    }



}
