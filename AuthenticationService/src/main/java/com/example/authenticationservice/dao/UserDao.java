package com.example.authenticationservice.dao;

import com.example.authenticationservice.entity.User;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserDao extends AbstractHibernateDao<User>{

    public UserDao() { setClazz(User.class);}


    public User getUserById(Integer id) {
        return findById(id);
    }

    public List<User> getAllUsers() {
        List<User> lst = this.getAll();
        return lst.stream()
                .map(u -> (User)u)
                .collect(Collectors.toList());
    }

    public Optional<User> loadUserByUsername(String username){
        return getAllUsers().stream().filter(user -> username.equals(user.getUsername())).findAny();
    }

}
