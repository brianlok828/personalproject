package com.example.authenticationservice.dao;

import com.example.authenticationservice.entity.Role;
import org.springframework.stereotype.Repository;


@Repository
public class RoleDao extends AbstractHibernateDao<Role>{
    public RoleDao() { setClazz(Role.class);}

}
