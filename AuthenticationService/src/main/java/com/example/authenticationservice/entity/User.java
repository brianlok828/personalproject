package com.example.authenticationservice.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name="user")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;
    @Column(name = "username")
    private String username;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "createDate")
    private Date createDate;
    @Column(name = "lastModificationDate")
    private Date lastModificationDate;
    @Column(name = "activeFlag")
    private boolean activeFlag;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    public Set<UserRole> userRoles;
}