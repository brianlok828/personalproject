package com.example.authenticationservice.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name="UserRole")
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;


//    @Column(name = "userId")
//    private Integer userId;
//    @Column(name = "roleId")
//    private Integer roleId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId")
    private User user;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "roleId")
    private Role role;

    @Column(name = "activeFlag")
    private boolean activeFlag;

    @Column(name = "createDate")
    private Date createDate;
    @Column(name = "lastModificationDate")
    private Date lastModificationDate;
}
