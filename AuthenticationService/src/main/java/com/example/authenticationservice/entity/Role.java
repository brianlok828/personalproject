package com.example.authenticationservice.entity;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name="role")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;
    @Column(name = "roleName")
    private String roleName;
    @Column(name = "roleDescription")
    private String roleDescription;
    @Column(name = "createDate")
    private Date createDate;
    @Column(name = "lastModificationDate")
    private Date lastModificationDate;


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "role")
    public Set<UserRole> orderItems;

}
