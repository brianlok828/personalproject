package com.example.authenticationservice.service;

import com.example.authenticationservice.dao.RegistrationTokenDao;
import com.example.authenticationservice.domain.RegTokenMessage;
import com.example.authenticationservice.entity.RegistrationToken;
import com.example.authenticationservice.util.SerializeUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.*;

@Service
public class RegistrationTokenService {
    private RegistrationTokenDao regDao;
    @Autowired
    public void setUserJdbcDao(RegistrationTokenDao regDao) {
        this.regDao = regDao;
    }

    private RabbitTemplate rabbitTemplate;
    @Autowired
    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public RegistrationToken generateRegistrationToken(String email, int userId) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.HOUR, 3);
        java.sql.Timestamp mySQLDate = new Timestamp(c.getTimeInMillis());

        String randChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder random = new StringBuilder();
        Random rnd = new Random();
        while (random.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * randChars.length());
            random.append(randChars.charAt(index));
        }
        String token = random.toString();

        RegTokenMessage message = RegTokenMessage.builder()
                .token(token)
                .email(email)
                .expirationDate(mySQLDate.toString())
                .build();

        String jsonMessage = SerializeUtil.serialize(message);
        rabbitTemplate.convertAndSend("tokenExchange", "token.key", jsonMessage);

        RegistrationToken regToken = RegistrationToken.builder()
                .token(token)
                .email(email)
                .expirationDate(mySQLDate)
                .createBy(userId)
                .build();
        regDao.add(regToken);
        return regToken;
    }

    //@Transactional
    public boolean validateRegistrationToken(String token, String email) {
        List<RegistrationToken> tokens = regDao.getAllRegistrationTokens();
        java.sql.Timestamp time = new Timestamp(System.currentTimeMillis());
        Optional<RegistrationToken> output = tokens.stream()
                .filter(t -> t.getEmail().equals(email))
                .filter(t -> t.getToken().equals(token))
                .filter(t -> t.getExpirationDate().after(time))
                .findAny();
        return output.isPresent();
    }

    public void deleteRegistrationToken(String token) {
        Optional<RegistrationToken> regToken = regDao.getRegistrationTokenByToken(token);
        System.out.println("deleting token");
        regDao.deleteRegistrationToken(token);
    }

}
