package com.example.authenticationservice.service;

import com.example.authenticationservice.dao.RoleDao;
import com.example.authenticationservice.dao.UserDao;
import com.example.authenticationservice.dao.UserRoleDao;
import com.example.authenticationservice.domain.response.UserResponse;
import com.example.authenticationservice.entity.Role;
import com.example.authenticationservice.entity.User;
import com.example.authenticationservice.entity.UserRole;
import com.example.authenticationservice.security.AuthUserDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

    private UserDao userDao;
    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
    private UserRoleDao userRoleDao;
    @Autowired
    public void setUserRoleDao(UserRoleDao userRoleDao) {
        this.userRoleDao = userRoleDao;
    }
    private RoleDao roleDao;
    @Autowired
    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    public User getUserById(int id) {
        return userDao.getUserById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = userDao.loadUserByUsername(username);

        if (!userOptional.isPresent()){
            throw new UsernameNotFoundException("Username does not exist");
        }

        User user = userOptional.get(); // database user

        return AuthUserDetail.builder() // spring security's userDetail
                .username(user.getUsername())
                .password(new BCryptPasswordEncoder().encode(user.getPassword()))
                .authorities(getAuthoritiesFromUser(user))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build();
    }

    //@Transactional
    public UserResponse addNewUser(String username, String email, String password) {
        java.util.Date javaDate = new java.util.Date();
        java.sql.Date mySQLDate = new java.sql.Date(javaDate.getTime());
        User user = User.builder()
                .username(username)
                .email(email)
                .password(password)
                .createDate(mySQLDate)
                .lastModificationDate(mySQLDate)
                .activeFlag(true)
                .build();
        userDao.add(user);
        UserRole userRole1 = UserRole.builder()
                .user(getUserById(userDao.loadUserByUsername(username).get().getId()))
                .role(roleDao.findById(1))
                .build();
        UserRole userRole2 = UserRole.builder()
                .user(getUserById(userDao.loadUserByUsername(username).get().getId()))
                .role(roleDao.findById(2))
                .build();
        UserRole userRole3 = UserRole.builder()
                .user(getUserById(userDao.loadUserByUsername(username).get().getId()))
                .role(roleDao.findById(3))
                .build();
        UserRole userRole4 = UserRole.builder()
                .user(getUserById(userDao.loadUserByUsername(username).get().getId()))
                .role(roleDao.findById(4))
                .build();
        userRoleDao.add(userRole1);userRoleDao.add(userRole2);userRoleDao.add(userRole3);userRoleDao.add(userRole4);
        return UserResponse.builder()
                .id(userDao.loadUserByUsername(username).get().getId())
                .username(username)
                .email(email)
                .password(password)
                .createDate(mySQLDate)
                .lastModificationDate(mySQLDate)
                .activeFlag(true)
                .build();
    }

    public boolean validateUsername(String username) {
        List<User> users = userDao.getAllUsers();
        return !users.stream().filter(u -> u.getUsername().equals(username)).findAny().isPresent();
    }

    public boolean validateEmail(String email) {
        List<User> users = userDao.getAllUsers();
        return !users.stream().filter(u -> u.getEmail().equals(email)).findAny().isPresent();
    }

    private List<GrantedAuthority> getAuthoritiesFromUser(User user){
        List<GrantedAuthority> userAuthorities = new ArrayList<>();

        for (String permission :  user.getUserRoles().stream()
                .map(u -> u.getRole().getRoleName()).collect(Collectors.toList()) ){
            userAuthorities.add(new SimpleGrantedAuthority(permission));
        }

        return userAuthorities;
    }

}
