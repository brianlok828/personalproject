package com.example.authenticationservice.exception;

public class InvalidUsernameException extends Exception{
    public InvalidUsernameException() {
        super("Invalid Username");
    }

}
