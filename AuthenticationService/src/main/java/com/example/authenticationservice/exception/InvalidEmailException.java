package com.example.authenticationservice.exception;

public class InvalidEmailException extends Exception {
    public InvalidEmailException() {
        super("Invalid Email");
    }
}
