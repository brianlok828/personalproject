package com.example.authenticationservice.exception;

public class InvalidRegTokenException extends Exception{
    public InvalidRegTokenException() {
        super("Invalid Registration Token");
    }

}
