package com.example.authenticationservice.controller;

import com.example.authenticationservice.domain.ServiceStatus;
import com.example.authenticationservice.domain.request.RegisterRequest;
import com.example.authenticationservice.domain.request.RegistrationTokenRequest;
import com.example.authenticationservice.domain.response.GetResponse;
import com.example.authenticationservice.domain.response.RegisterResponse;
import com.example.authenticationservice.domain.response.RoleResponse;
import com.example.authenticationservice.domain.response.UserResponse;
import com.example.authenticationservice.entity.RegistrationToken;
import com.example.authenticationservice.entity.User;
import com.example.authenticationservice.entity.UserRole;
import com.example.authenticationservice.exception.InvalidEmailException;
import com.example.authenticationservice.exception.InvalidRegTokenException;
import com.example.authenticationservice.exception.InvalidUsernameException;
import com.example.authenticationservice.service.RegistrationTokenService;
import com.example.authenticationservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("auth")
public class UserController {
    UserService userService;
    RegistrationTokenService regService;
    public UserController(UserService userService, RegistrationTokenService regService) {
        this.userService = userService;
        this.regService = regService;
    }

    @GetMapping("/user{id}")
    public GetResponse getUserById(@PathVariable int id) {
        User user = userService.getUserById(id);
        List<RoleResponse> roleResponses = new ArrayList<>();
        for(UserRole userRole : user.getUserRoles()) {
            roleResponses.add(RoleResponse.builder().roleName(userRole.getRole().getRoleName()).build());
        }
        return GetResponse.builder()
                .status(ServiceStatus.builder().success(true).build())
                .userResponse(UserResponse.builder()
                        .id(id)
                        .username(user.getUsername())
                        .email(user.getEmail())
                        .password(user.getPassword())
                        .createDate(user.getCreateDate())
                        .lastModificationDate(user.getLastModificationDate())
                        .activeFlag(user.isActiveFlag())
                        .roleResponses(roleResponses)
                        .build()
                )
                .build();
    }

    @PostMapping("/register")
    public RegisterResponse registerNewUser(@RequestBody RegisterRequest request) {
        try{
            //check if reg token is valid
            if(!regService.validateRegistrationToken(request.getToken(), request.getEmail())) {
                throw new InvalidRegTokenException();
            }else if(!userService.validateUsername(request.getUsername())) {
                throw new InvalidUsernameException();
            }else if(!userService.validateEmail(request.getEmail())) {
                throw new InvalidEmailException();
            }else {
                UserResponse output = userService.addNewUser(request.getUsername(), request.getEmail(), request.getPassword());

                return RegisterResponse.builder()
                        .status(ServiceStatus.builder().success(true).build())
                        .userResponse(output)
                        .build();
            }
        }catch (Exception e) {
            e.printStackTrace();
            return RegisterResponse.builder()
                    .status(ServiceStatus.builder().success(false).message(e.getMessage()).build())
                    .build();
        }
    }

    @PostMapping("/generate-registration-token")
    public RegistrationToken generateRegistrationToken(@RequestBody RegistrationTokenRequest request) {
        return regService.generateRegistrationToken(request.getEmail(), request.getUserId());
    }
}
