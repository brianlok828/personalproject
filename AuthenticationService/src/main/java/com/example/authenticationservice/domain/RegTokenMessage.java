package com.example.authenticationservice.domain;

import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@Builder
@ToString
public class RegTokenMessage implements Serializable {
    private String token;
    private String email;
    private String expirationDate;
}
