package com.example.authenticationservice.domain.request;
import lombok.*;

@Getter
@Setter
public class RegistrationTokenRequest {
    String email;
    Integer userId;
}
