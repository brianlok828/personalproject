package com.example.authenticationservice.domain.request;
import lombok.*;

@Getter
@Setter
@Builder
public class RegisterRequest {
    String token;
    String username;
    String email;
    String password;
}
