package com.example.authenticationservice.domain.response;
import com.example.authenticationservice.entity.Role;
import lombok.*;

import java.sql.Date;
import java.util.List;

@Getter
@Setter
@Builder
public class UserResponse {
    Integer id;
    String username;
    String email;
    String password;
    Date createDate;
    Date lastModificationDate;
    boolean activeFlag;
    List<RoleResponse> roleResponses;
}
