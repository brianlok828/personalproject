package com.example.authenticationservice.domain.response;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
public class RoleResponse {
    String roleName;
}
