package com.example.authenticationservice.domain.response;
import com.example.authenticationservice.domain.ServiceStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RegisterResponse {
    ServiceStatus status;
    UserResponse userResponse;
}
