package com.example.authenticationservice.domain.response;
import com.example.authenticationservice.domain.ServiceStatus;
import com.example.authenticationservice.entity.User;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
public class GetResponse {
    ServiceStatus status;
    UserResponse userResponse;
}
