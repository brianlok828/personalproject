package com.example.authenticationservice.service;

import com.example.authenticationservice.controller.UserController;
import com.example.authenticationservice.dao.UserDao;
import com.example.authenticationservice.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserService userService;

    @Test
    void test_getUserById() {
        User expected = User.builder()
                .id(1)
                .username("user1")
                .email("email1")
                .password("pass1")
                .userRoles(null)
                .build();
        Mockito.when(userDao.getUserById(1)).thenReturn(expected);
        assertEquals(expected.getId(), userService.getUserById(1).getId());
        assertEquals(expected.getUsername(), userService.getUserById(1).getUsername());
        assertEquals(expected.getPassword(), userService.getUserById(1).getPassword());
        assertEquals(expected.getEmail(), userService.getUserById(1).getEmail());
    }
}
