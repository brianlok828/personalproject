package com.example.authenticationservice.controller;

import com.example.authenticationservice.controller.UserController;
import com.example.authenticationservice.domain.ServiceStatus;
import com.example.authenticationservice.domain.request.RegisterRequest;
import com.example.authenticationservice.domain.response.GetResponse;
import com.example.authenticationservice.domain.response.RegisterResponse;
import com.example.authenticationservice.domain.response.UserResponse;
import com.example.authenticationservice.security.JwtFilter;
import com.example.authenticationservice.security.JwtProvider;
import com.example.authenticationservice.service.RegistrationTokenService;
import com.example.authenticationservice.service.UserService;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(controllers = UserController.class)
@AutoConfigureMockMvc(addFilters = false)
public class UserControllerTest {
    @MockBean
    private JwtFilter jwtFilter;
    @MockBean
    private JwtProvider jwtProvider;
    @MockBean
    private UserService userService;
    @MockBean
    private RegistrationTokenService regService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void test_registerNewUser() throws Exception{
        RegisterResponse expectedRegisterResponse = RegisterResponse.builder()
                .status(ServiceStatus.builder().success(false).build())
                .build();
        //Mockito.when(userService.addNewUser("test username", "test email", "test password")).thenReturn(expectedUserResponse);

        RegisterRequest registerRequest = RegisterRequest.builder()
                .token("sample token")
                .username("test username")
                .email("sample email")
                .password("test password")
                .build();

        Gson gson = new Gson();
        //String responseJson = gson.toJson(expectedRegisterResponse);
        String requestJson = gson.toJson(registerRequest);
        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.post("/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andReturn();
        // use Gson to convert the JSON response to a Product object
        RegisterResponse actual = gson.fromJson(result.getResponse().getContentAsString(), RegisterResponse.class);
        assertEquals(expectedRegisterResponse.getStatus().isSuccess(), actual.getStatus().isSuccess());
        assertEquals(expectedRegisterResponse.getUserResponse(), actual.getUserResponse());
    }



}
