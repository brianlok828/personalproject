package com.beaconfire.rabbitmqconsumer;

import com.google.gson.Gson;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Properties;

public class RabbitListener implements MessageListener {
    @Override
    public void onMessage(Message message) {
        System.out.println("New message received from "
                + message.getMessageProperties().getConsumerQueue()
                + ": "
                + new String(message.getBody()));

        // sending email
        Properties props = new Properties();
        props.put("mail.smtp.ssl.enable", "true");
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(465);
        mailSender.setUsername("jerry2000wang@gmail.com");
        mailSender.setPassword("oqvwjlkaaallmcya");
        mailSender.setJavaMailProperties(props);

        SimpleMailMessage msg = new SimpleMailMessage();
        if(message.getMessageProperties().getConsumerQueue().equals("tokenQueue")) {
            Gson g = new Gson();
            CustomMessage customMessage = g.fromJson(new String(message.getBody()), CustomMessage.class);

            msg.setFrom("jerry2000wang@gmail.com");
            msg.setTo(customMessage.getEmail());
            msg.setSubject("Registration Token");
            msg.setText("Here is your registration token: " + customMessage.getToken() + "\nIt expires at: " + customMessage.getExpirationDate());
        }else {
            msg.setFrom("jerry2000wang@gmail.com");
            msg.setTo(new String(message.getBody()));
            msg.setSubject("Your Application was Rejected");
            msg.setText("Your application was rejected, please log in to view status and comments");
        }

        try{
            mailSender.send(msg);
            System.out.println("Email sent successfully");
        }
        catch(MailException ex) {
            System.err.println(ex.getMessage());
        }

    }
}
