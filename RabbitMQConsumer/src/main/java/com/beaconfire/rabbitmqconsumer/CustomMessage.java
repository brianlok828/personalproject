package com.beaconfire.rabbitmqconsumer;

import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@ToString
public class CustomMessage {
    private String token;
    private String email;
    private String expirationDate;
}
