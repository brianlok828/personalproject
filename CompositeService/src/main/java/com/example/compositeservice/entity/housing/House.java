package com.example.compositeservice.entity.housing;

import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class House {
    private Integer id;

    private Integer landlordId;

    private String address;

    private Integer maxOccupant;

    Landlord landlord;

    Facility facility;

}