package com.example.compositeservice.entity.housing;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Landlord {
    private Integer landlordID;

    private String firstName;

    private String lastName;

    private String email;

    private String cellPhone;

}