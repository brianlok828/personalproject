package com.example.compositeservice.entity.application;

import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class ApplicationWorkFlow {

//    @Column(name = "ApplicationWorkFlow_id")
    private Integer applicationWorkFlow_id;

    private String employeeId;

    private Date createDate;

    private Date lastModificationDate;

    private String status;

    private String comment;
}
