package com.example.compositeservice.entity.employee;

import java.util.Date;
import java.util.List;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    private String id;
    private String userId;
    private String firstName;
    private String lastName;
    private String middleName;
    private String preferredName;
    private String email;
    private String cellPhone;
    private String alternatePhone;
    private String gender;
    private String ssn;
    private Date dob;
    private Date startDate;
    private Date endDate;
    private String driverLicense;
    private Date driverLicenseExpiration;
    private Integer houseId;
    private List<Contact> contactList;
    private List<Address> addressList;
    private List<VisaStatus> visaStatusList;
    private List<PersonalDocument> personalDocumentList;
}
