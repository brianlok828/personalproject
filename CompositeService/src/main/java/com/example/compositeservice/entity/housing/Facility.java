package com.example.compositeservice.entity.housing;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Facility {
    private Integer facilityId;

    private Integer houseId;

    private String type;

    private String description;

    private Integer quantity;
}
