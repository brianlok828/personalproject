package com.example.compositeservice.entity.employee;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class VisaStatus {
    private String id;

    private String visaType;

    private boolean activeFlag;

    private Date startDate;

    private Date endDate;

    private Date lastModificationDate;
}
