package com.example.compositeservice.entity.employee;

import lombok.*;

import java.nio.file.Path;
import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PersonalDocument {
    private Integer id;

    //Path not working
    private String path;

    private String title;

    private String comment;

    private Date createDate;
}
