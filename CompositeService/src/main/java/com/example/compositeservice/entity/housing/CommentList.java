package com.example.compositeservice.entity.housing;
import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentList {
    private Integer id;
    private Integer facilityReportId;
    private String reportComment;
    private String description;
    private int createdBy;

    private Timestamp createDate;
}
