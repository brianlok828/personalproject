package com.example.compositeservice.controller;

import com.example.compositeservice.domain.application.ApplicationWorkFlowResponse;
import com.example.compositeservice.domain.employee.EmployeeResponse;
import com.example.compositeservice.domain.housing.HousingSummaryResponse;
import com.example.compositeservice.service.CompositeService;
import com.example.compositeservice.domain.housing.HouseDetailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CompositeController {
    private CompositeService compositeService;

    @Autowired
    public void setCompositeService(CompositeService compositeService) {
        this.compositeService = compositeService;
    }

    @GetMapping("/house-details")
    public HouseDetailResponse getHouseDetails(@RequestHeader(value = "Authorization") String bearer, @RequestParam String employeeId) {
        return compositeService.getHouseDetails(bearer, employeeId);
    }

    @GetMapping("/house-summary")
    public HousingSummaryResponse getHousingSummary(@RequestHeader(value = "Authorization") String bearer, @RequestParam Integer houseId) {
        return compositeService.getHousingSummary(bearer, houseId);
    }

    @PostMapping("/assign-house-employee")
    public EmployeeResponse assignRandomHouseToEmployee(@RequestHeader(value = "Authorization") String bearer, @RequestParam String employeeId) {
        return compositeService.assignRandomHouseToEmployee(bearer, employeeId);
    }

    @PatchMapping("/update-application-status")
    public ApplicationWorkFlowResponse updateApplicationStatus(@RequestHeader(value = "Authorization") String bearer, @RequestParam Integer id, @RequestParam String status, @RequestParam String comment) {
        return compositeService.updateStatusApplicationWorkflow(bearer, id, status, comment);
    }
}
