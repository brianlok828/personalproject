package com.example.compositeservice.service;

import com.example.compositeservice.domain.application.ApplicationWorkFlowResponse;
import com.example.compositeservice.domain.employee.*;
import com.example.compositeservice.domain.housing.FacilityInfoResponse;
import com.example.compositeservice.domain.housing.HouseDetailResponse;
import com.example.compositeservice.domain.housing.HouseResponse;
import com.example.compositeservice.domain.housing.HousingSummaryResponse;
import com.example.compositeservice.entity.application.ApplicationWorkFlow;
import com.example.compositeservice.entity.employee.Employee;
import com.example.compositeservice.entity.housing.House;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CompositeService {
    private RemoteHousingService housingService;
    private RemoteEmployeeService employeeService;
    private RemoteApplicationService applicationService;

    @Autowired
    public void setHousingService(RemoteHousingService housingService) {
        this.housingService = housingService;
    }
    @Autowired
    public void setEmployeeService(RemoteEmployeeService employeeService) {
        this.employeeService = employeeService;
    }
    @Autowired
    public void setApplicationService(RemoteApplicationService applicationService) {this.applicationService = applicationService;}

    private RabbitTemplate rabbitTemplate;
    @Autowired
    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public HouseDetailResponse getHouseDetails(String bearer, String employeeId) {
        EmployeeResponse employeeResponse = employeeService.getEmployeeById(bearer, employeeId);
        Integer houseId = employeeResponse.getEmployee().getHouseId();
        System.out.println(houseId);
        HouseResponse houseResponse = housingService.getHouseByIdEmployee(bearer, houseId);
        AllEmployeesResponse response = employeeService.getEmployeeByHouseId(bearer, houseId);
        List<RoommateResponse> roommates = new ArrayList<>();
        for(Employee e : response.getEmployees()) {
            roommates.add(RoommateResponse.builder()
                            .name(e.getPreferredName().isEmpty() ? (e.getFirstName() + e.getLastName()) : e.getPreferredName())
                            .phoneNumber(e.getCellPhone())
                            .build());
        }
        return HouseDetailResponse.builder()
                .address(houseResponse.getAddress())
                .roommates(roommates)
                .build();
    }

    public HousingSummaryResponse getHousingSummary(String bearer, Integer houseId) {
        House house = housingService.getHouseById(bearer, houseId);
        AllEmployeesResponse response = employeeService.getEmployeeByHouseId(bearer, houseId);
        List<EmployeeInfoResponse> employeeInfoResponses = new ArrayList<>();
        for(Employee e : response.getEmployees()) {
            employeeInfoResponses.add(EmployeeInfoResponse.builder()
                            .fullName(e.getPreferredName().isEmpty() ? (e.getFirstName() + e.getLastName()) : e.getPreferredName())
                            .email(e.getEmail())
                            .phoneNumber(e.getCellPhone())
                            .driverLicense(e.getDriverLicense())
                            .driverLicenseExpiration(e.getDriverLicenseExpiration() != null ? e.getDriverLicenseExpiration().toString() : null)
                            .build());
        }
        return HousingSummaryResponse.builder()
                .address(house.getAddress())
                .landlord(housingService.getLandlordById(bearer, house.getLandlordId()))
                .numResidents(response.getEmployees().size())
                .facilityInfoResponse(housingService.getFacilityViewByHouseId(bearer, houseId))
                .employeeInfoResponseList(employeeInfoResponses)
                .build();
    }

    public EmployeeResponse assignRandomHouseToEmployee(String bearer, String employeeId) {
        Optional<House> house = housingService.getAllHouse(bearer).stream()
                .filter(h -> h.getMaxOccupant() > employeeService.getEmployeeByHouseId(bearer, h.getId()).getEmployees().size())
                .findAny();
        if(house.isPresent()) {
            return employeeService.assignHouseToEmployee(bearer, employeeId, house.get().getId());
        }else {
            return EmployeeResponse.builder()
                    .status(ResponseStatus.builder().message("No available house").success(false).build())
                    .build();
        }
    }

    public ApplicationWorkFlowResponse updateStatusApplicationWorkflow(String bearer, Integer id, String status, String comment) {
        ApplicationWorkFlow awf = ApplicationWorkFlow.builder()
                .applicationWorkFlow_id(id)
                .status(status)
                .comment(comment)
                .build();
        ApplicationWorkFlowResponse response = applicationService.updateStatusApplicationWorkflow(bearer, awf);
        if(status.equals("rejected")) {
            //System.out.println("preparing reject email");
            String email = employeeService.getEmployeeById(bearer, response.getApplicationWorkFlow().getEmployeeId()).getEmployee().getEmail();
            rabbitTemplate.convertAndSend("rejectExchange", "reject.key", email);
        }
        return response;
    }

}
