package com.example.compositeservice.service;

import com.example.compositeservice.domain.application.ApplicationWorkFlowResponse;
import com.example.compositeservice.entity.application.ApplicationWorkFlow;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("application-service")
public interface RemoteApplicationService {

    @PostMapping("/application-service/applicationWorkFlow/update")
    ApplicationWorkFlowResponse updateStatusApplicationWorkflow(@RequestHeader(value = "Authorization") String bearer, @RequestBody ApplicationWorkFlow awf);
}
