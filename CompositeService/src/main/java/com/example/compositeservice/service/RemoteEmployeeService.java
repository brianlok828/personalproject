package com.example.compositeservice.service;

import com.example.compositeservice.domain.employee.AllEmployeesResponse;
import com.example.compositeservice.domain.employee.EmployeeResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("employee-service")
public interface RemoteEmployeeService {
    @GetMapping("/employee-service/employee/{employeeId}")
    EmployeeResponse getEmployeeById(@RequestHeader(value = "Authorization") String bearer, @PathVariable String employeeId);

    @GetMapping("/employee-service/employees/{houseId}")
    AllEmployeesResponse getEmployeeByHouseId(@RequestHeader(value = "Authorization") String bearer, @PathVariable Integer houseId);

    @PostMapping("/employee-service/employee/{employeeId}/assignHouse")
    EmployeeResponse assignHouseToEmployee(@RequestHeader(value = "Authorization") String bearer, @PathVariable String employeeId, @RequestParam Integer houseId);
}
