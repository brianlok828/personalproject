package com.example.compositeservice.service;

import com.example.compositeservice.domain.housing.FacilityInfoResponse;
import com.example.compositeservice.domain.housing.HouseResponse;
import com.example.compositeservice.entity.housing.House;
import com.example.compositeservice.entity.housing.Landlord;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("housing-service")
public interface RemoteHousingService {

    @GetMapping("/housing-service/house")
    List<House> getAllHouse(@RequestHeader(value = "Authorization") String bearer);
    @GetMapping("/housing-service/facilityView")
    FacilityInfoResponse getFacilityViewByHouseId(@RequestHeader(value = "Authorization") String bearer, @RequestParam Integer houseId);

    @GetMapping("/housing-service/employee/houseId")
    HouseResponse getHouseByIdEmployee(@RequestHeader(value = "Authorization") String bearer, @RequestParam Integer houseId);

    @GetMapping("/housing-service/houseId")
    House getHouseById(@RequestHeader(value = "Authorization") String bearer, @RequestParam Integer houseId);

    @GetMapping("/housing-service/landlord")
    Landlord getLandlordById(@RequestHeader(value = "Authorization") String bearer, @RequestParam Integer id);
}
