package com.example.compositeservice.domain.employee;

import com.example.compositeservice.entity.employee.Employee;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class EmployeeResponse {
    private ResponseStatus status;
    private Employee employee;
}