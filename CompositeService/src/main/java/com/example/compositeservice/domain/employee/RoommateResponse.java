package com.example.compositeservice.domain.employee;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoommateResponse {
    String name;
    String phoneNumber;
}
