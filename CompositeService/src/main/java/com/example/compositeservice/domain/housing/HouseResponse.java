package com.example.compositeservice.domain.housing;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HouseResponse {
    String address;
}
