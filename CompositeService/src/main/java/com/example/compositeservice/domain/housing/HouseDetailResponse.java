package com.example.compositeservice.domain.housing;

import com.example.compositeservice.domain.employee.RoommateResponse;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HouseDetailResponse {
    private String address;
    private List<RoommateResponse> roommates;
}
