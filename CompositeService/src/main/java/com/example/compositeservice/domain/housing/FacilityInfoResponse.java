package com.example.compositeservice.domain.housing;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FacilityInfoResponse {
    int numBeds;
    int numMattresses;
    int numTables;
    int numChairs;
    List<FacilityReportResponse> facilityReports;
}
