package com.example.compositeservice.domain.housing;

import com.example.compositeservice.domain.employee.EmployeeInfoResponse;
import com.example.compositeservice.entity.housing.Landlord;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HousingSummaryResponse {
    String address;
    Landlord landlord;
    int numResidents;
    FacilityInfoResponse facilityInfoResponse;
    List<EmployeeInfoResponse> employeeInfoResponseList;
}
