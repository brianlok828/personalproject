package com.example.compositeservice.domain.housing;

import com.example.compositeservice.entity.housing.CommentList;
import lombok.*;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FacilityReportResponse {
    String title;
    String description;
    int createdBy;
    Timestamp createdTime;
    List<CommentList> commentList;
}
