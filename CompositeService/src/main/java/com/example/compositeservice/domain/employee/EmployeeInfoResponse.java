package com.example.compositeservice.domain.employee;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeInfoResponse {
    String fullName;
    String phoneNumber;
    String email;
    String driverLicense;
    String driverLicenseExpiration;
}
