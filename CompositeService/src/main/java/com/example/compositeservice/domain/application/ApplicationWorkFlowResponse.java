package com.example.compositeservice.domain.application;

import com.example.compositeservice.domain.employee.ResponseStatus;
import com.example.compositeservice.entity.application.ApplicationWorkFlow;
import lombok.*;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationWorkFlowResponse {
    private ResponseStatus responseStatus;
    private ApplicationWorkFlow applicationWorkFlow;
}
