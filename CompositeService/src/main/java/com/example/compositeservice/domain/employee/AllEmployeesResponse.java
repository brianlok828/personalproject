package com.example.compositeservice.domain.employee;

import com.example.compositeservice.entity.employee.Employee;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class AllEmployeesResponse {
    private ResponseStatus status;
    private List<Employee> employees;
}
